'''
Created on Nov 26, 2013

@author: vaibhavsaini
'''
import os
import random
from random import shuffle
import sys
import math
class Preprocessor(object):
    '''
    classdocs
    '''


    def __init__(self, params):
        '''
        Constructor
        '''
        self.filenamePrefix = "train"
        self.inputFolder = "../data"
        self.outputFolder= "../output"
        self.sourceFile = "{0}/{1}.csv".format(self.inputFolder,self.filenamePrefix)
        self.destFilePrefix = "{0}/{1}".format(self.outputFolder,self.filenamePrefix)
        #self.wekaFileTest = "{0}/{1}{2}-test.arff".format(self.outputFolder,self.filenamePrefix,self.recordsLimit)

    def writeToFile(self, filename, text):
        print filename
        f = open(filename, 'w+')
        f.write(text)
        f.close()

    def createTrainSetOnly(self):
        f = open(self.sourceFile)
        lines = f.readlines()
        indexesRead = {}
        text = lines[0]
        indexesRead[0] = 1
        count = 0
        while True:
            if count % 10 == 1:
                print "{0} lines written".format(count)
            index = random.randint(1, len(lines) - 1)
            if index in indexesRead:
                continue
            else:
                indexesRead[index] = 1
                line = lines[index]
                print line
                text = text + line
                count += 1
                if count >= self.recordsLimit:
                    break
        destFile = "{0}{1}-trainOnly.csv".format(self.destFilePrefix, self.recordsLimit)
        self.writeToFile(destFile, text)
        wekaFileTrain = "{0}/{1}{2}-trainOnly.arff".format(self.outputFolder,self.filenamePrefix,self.recordsLimit)
        self.generateArff(destFile,wekaFileTrain)
        
    def createTrainTest(self, trainSplit=80, limitRecords=None):
        # limitRecords to consider
        # trainTestSplit : 20   , means 20 - 80 split.
        trainSplit = abs(trainSplit)
        f = open(self.sourceFile)
        lines = f.readlines()
        header = lines[0]
        lines = lines[1:]
        shuffle(lines)
        trainRecords = []
        testRecords= []
        if limitRecords and limitRecords<=len(lines):
            records = lines[:limitRecords]
            trainIndex = int(math.ceil(trainSplit*.01*len(records)))
            print "trainIndex:" , trainIndex
            trainRecords.append(header)
            trainRecords.extend(records[:trainIndex])
            testRecords.append(header)
            testRecords.extend(records[trainIndex:])
            csvFileTrain = "{0}{1}-{2}-trainsplit.csv".format(self.destFilePrefix, trainSplit,limitRecords)
            csvFileTest = "{0}{1}-{2}-testsplit.csv".format(self.destFilePrefix, trainSplit,limitRecords)
            wekaFileTrainSplit = "{0}/{1}{2}-{3}-trainsplit.arff".format(self.outputFolder,self.filenamePrefix,trainSplit,limitRecords)
            wekaFileTestSplit = "{0}/{1}{2}-{3}-testsplit.arff".format(self.outputFolder,self.filenamePrefix,trainSplit,limitRecords)
            self.writeListToFile(trainRecords, csvFileTrain)
            self.writeListToFile(testRecords, csvFileTest)
            self.generateArff(csvFileTrain, wekaFileTrainSplit)
            self.generateArff(csvFileTest, wekaFileTestSplit)
    
    def writeListToFile(self, list, filename):
        text = ""
        for line in list:
            if line.split(",")[0]=='50000':
                continue
                #line = line+"\n"
            text = text + line
        self.writeToFile(filename, text)
        

    def generateArff(self,csvFile, arffFile):
        cmd ="java -Xms13g -Xmx13g -classpath weka.jar weka.core.converters.CSVLoader {0} > {1}".format(csvFile,arffFile)
        print cmd
        os.system(cmd)

if __name__ == '__main__':
    args = sys.argv
    print"starting..."
    if args[1] == 'c':
        print "creating test and train set"
        preprocessor = Preprocessor({})
        preprocessor.createTrainTest(int(args[2]), int(args[3]))
    else:
        print "creating a train set only"
        preprocessor = Preprocessor()
        preprocessor.recordsLimit = int(args[1])
        preprocessor.createTrainSetOnly();
        print "generating arff file:", preprocessor.wekaFile
        #preprocessor.generateArff();
    print "done"
